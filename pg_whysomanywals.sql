
-- Compilation of different informations on the WALs retention
-- including archiving, slot, archiving from slave
--
-- For PG v10+
--
-- 
--
WITH stg AS (SELECT name,
            setting::bigint * CASE unit WHEN '8kB' THEN 8192 WHEN 'B' THEN 1 WHEN 'MB' THEN 1024*1024 ELSE null END AS v
            FROM pg_settings
            WHERE name IN ('wal_segment_size', 'wal_keep_size', 'max_slot_wal_keep_size', 'max_wal_size', 'min_wal_size') 
),
c AS ( /* present situation & parameters, 1 line */
    SELECT  is_master, current_setting ('cluster_name') AS cluster_name ,
            CASE is_master WHEN true THEN pg_current_wal_lsn () ELSE pg_last_wal_replay_lsn () END AS current_lsn,
            CASE is_master WHEN true THEN pg_walfile_name (pg_current_wal_lsn ()) END AS current_wal,
            CASE NOT is_master WHEN true THEN pg_last_xact_replay_timestamp () END AS last_replay_ts,
            archive_mode,
            trim(archive_command) AS archive_command,  -- TODO : archive_library?
            CASE archive_mode = 'always' OR (archive_mode = 'on' AND is_master)
                 WHEN true THEN CASE WHEN archive_command = '/bin/true' THEN 'Disabled (/bin/true)'
                            ELSE 'Active' || CASE is_master WHEN false THEN ' (secondary)' ELSE '' END
                            END 
                 ELSE 'Off' END AS archiving_status,
            (SELECT v FROM stg WHERE name = 'wal_segment_size') AS wal_segment_size, /*usually 16 MB */
            coalesce((SELECT v FROM stg WHERE name = 'wal_keep_size'),0) AS wal_keep_size,
            (SELECT v FROM stg WHERE name = 'max_slot_wal_keep_size') AS max_slot_wal_keep_size,  -- v13+
            (SELECT v FROM stg WHERE name = 'max_wal_size') AS max_wal_size,
            (SELECT v FROM stg WHERE name = 'min_wal_size') AS min_wal_size
            FROM (SELECT  NOT pg_is_in_recovery () AS is_master,
                    current_setting ('archive_mode') AS archive_mode,
                    current_setting ('archive_command') AS archive_command
                    ) o
),
wals AS (  /* wal past and present, ignore future and cruft on master ; all on slave */ 
    SELECT *, ( jnl <= (SELECT COALESCE (c.current_wal,'9') FROM c)) AS is_past
    FROM pg_ls_dir ('pg_wal') AS jnl
    WHERE jnl ~ '^[0-9A-F]{24}$'
),
wals_agg AS (  /* 1 line */ 
    SELECT  COUNT(*) FILTER (WHERE is_past) AS nb_past_wals,
            COUNT(*) AS nb_total_wals,
            MIN(jnl) AS oldest_wal,
            MAX (jnl) FILTER (WHERE is_past) AS last_past_wal,
            MAX (jnl) AS last_wal
    FROM wals
),
arch AS (
    SELECT  last_archived_wal,
            (SELECT COUNT(*) FROM wals WHERE jnl > last_archived_wal AND is_past) AS nb_wals_to_archive,
            last_archived_time,
            now()-last_archived_time as delay_since_last_arch_success,
            failed_count,  last_failed_wal, last_failed_time, stats_reset,
            (coalesce(last_failed_wal,'') > coalesce(last_archived_wal,'9')) AS archiving_is_failing,
            now()-last_failed_time as delay_since_last_arch_failure
    FROM pg_stat_archiver, c
),
worst_slot AS ( /* worst slot */
    SELECT  s.slot_name,restart_lsn,
            pg_wal_lsn_diff (c.current_lsn, s.restart_lsn) AS slot_delay_size,
            s.temporary AS slot_is_temporary,
            r.application_name,
            trim(coalesce (r.client_hostname,'')||' '||coalesce(r.client_addr::text,'')) AS client
    FROM    pg_replication_slots s
    LEFT OUTER JOIN pg_stat_replication r ON (r.pid=s.active_pid), c
    ORDER   BY xmin::text::bigint ASC NULLS LAST  LIMIT 1  /* oldest slot */
)
SELECT
    -- general info
    cluster_name,
    nb_total_wals ||' ('||oldest_wal||' - '|| last_wal||')' AS wals_on_disk,
    nb_past_wals ||' ('||oldest_wal||' - '|| last_past_wal||')' AS past_wals_on_disk,
    'all: '|| pg_size_pretty (nb_total_wals * wal_segment_size)
    ||' / past: '|| pg_size_pretty (nb_past_wals * wal_segment_size)
        ||'   (min/max: '|| pg_size_pretty(min_wal_size)||' / '||pg_size_pretty(max_wal_size)
        || CASE WHEN wal_keep_size>0 THEN ', kept: '|| pg_size_pretty(wal_keep_size) ELSE '' END
        ||')' AS past_wals_on_disk_size,
    current_lsn ||'    (' ||
    CASE is_master WHEN true THEN current_wal
    ELSE last_replay_ts::text END ||')' AS current_lsn ,
    -- archiving
    archiving_status || CASE 
    WHEN archiving_status LIKE 'Active%' THEN ' / '
       || CASE archiving_is_failing WHEN true THEN '!!! FAILING !!!'
          WHEN false THEN
           -- TODO : another formula to detect a slow archiving?
              CASE WHEN nb_wals_to_archive * wal_segment_size >= (max_wal_size-wal_keep_size) THEN '*LAGGING...*'
              ELSE 'OK' END
           ELSE '? state ?' END 
    || ' (last: '||last_archived_wal||')'  
    ELSE '' END AS archiving_status,
    nb_wals_to_archive || ' ('|| pg_size_pretty (nb_wals_to_archive * wal_segment_size) ||')' AS nb_wals_to_archive,
    CASE archiving_is_failing WHEN true THEN last_failed_time WHEN false THEN last_archived_time END AS last_arch_success_or_failure,
    CASE WHEN archiving_status != 'Off' THEN archive_command END AS archive_command,
    -- slots
    ws.slot_name||CASE slot_is_temporary WHEN true THEN ' ( *temp* )' ELSE '' END AS worst_replication_slot,
    trim (ws.application_name ||' '|| case ws.client when '' then '(unknown client)' else ws.client end) AS slot_app_and_client,
    pg_size_pretty (slot_delay_size) || ' (' || ceil (slot_delay_size / wal_segment_size) ||' wals)' AS slot_delay,
    CASE WHEN ws.slot_name IS NULL THEN null
         WHEN max_slot_wal_keep_size < 0 THEN '∞'
         ELSE pg_size_pretty (max_slot_wal_keep_size) END AS max_slot_retention
FROM 
    arch, wals_agg, c LEFT OUTER JOIN worst_slot ws ON (true)
\gx
