SQL query to detect why there are so many wals on the server: archiving problem or stuck slot?

Examples:

```bash
psql  < pg_whysomanywals/pg_whysomanywals.sql
```
```
═[ RECORD 1 ]════════════════╦═══════════════════════════════════════════════════════════════════════════════════════════════════════════
cluster_name                 ║ 14/fourmi1
past_wals_on_disk            ║ 35 (000000070000001000000005 - 000000070000001000000027)
past_wals_on_disk_size       ║ 560 MB   (min/max: 80 MB / 1024 MB, kept: 128 MB)
current_lsn                  ║ 10/28000000    (000000070000001000000027)
archiving_status             ║ Active / OK (last: 000000070000001000000026)
nb_wals_to_archive           ║ 1
to_archive_size              ║ 16 MB
last_arch_success_or_failure ║ 2023-03-09 15:52:28.886857+01
archive_command              ║ /usr/bin/pgbackrest --config=/etc/postgresql/14/fourmi1/pgbackrest.conf --stanza=fourmi1   archive-push %p
worst_replication_slot       ║ fourmi3
slot_app_and_client          ║ 14/fourmi3 (unknown client)
slot_delay                   ║ 16 MB (1 wals)
max_slot_retention           ║ 3072 MB
```

```
═[ RECORD 1 ]════════════════╦══════════════════════════════════════════════════════════
cluster_name                 ║ 14/fourmi1
past_wals_on_disk            ║ 108 (000000070000001000000064 - 0000000700000010000000CF)
past_wals_on_disk_size       ║ 1728 MB   (min/max: 80 MB / 1024 MB, kept: 128 MB)
current_lsn                  ║ 10/D0000000    (0000000700000010000000CF)
archiving_status             ║ Active / !!! FAILING !!! (last: 0000000700000010000000C2)
nb_wals_to_archive           ║ 13
to_archive_size              ║ 208 MB
last_arch_success_or_failure ║ 2023-03-09 15:55:33.630603+01
archive_command              ║ /bin/false
worst_replication_slot       ║ fourmi3
slot_app_and_client          ║ ø
slot_delay                   ║ 1726 MB (108 wals)
max_slot_retention           ║ 3072 MB
```

```
═[ RECORD 1 ]════════════════╦═══════════════════════════════════════════════════════════════════
cluster_name                 ║ 14/fourmi3
past_wals_on_disk            ║ 574 (000000070000000E000000C0 - 0000000700000010000000FD)
past_wals_on_disk_size       ║ 9184 MB   (min/max: 80 MB / 111 MB)
current_lsn                  ║ 10/FD15FFE0    (2023-03-09 15:59:07.692781+01)
archiving_status             ║ Active (secondary) / *LAGGING...* (last: 000000070000000E000000C8)
nb_wals_to_archive           ║ 565
to_archive_size              ║ 9040 MB
last_arch_success_or_failure ║ 2023-03-09 15:59:08.330524+01
archive_command              ║ touch /tmp/fourmi3.arch ; sleep 5 ; /bin/true
worst_replication_slot       ║ fourmi2
slot_app_and_client          ║ 14/fourmi2 (unknown client)
slot_delay                   ║ 11 MB (1 wals)
```
